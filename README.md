# Why testing code?

-   To make sure the code behaves as expected (better to catch bugs in dev than in prod)
-   To make sure new code doesn't break existing one


# Unit testing

Unit testing refers to the practice of testing code as a unit, without regards to other services that may use or be used by the code.

Usually unit testing is the first step before proceeding to integration tests, testing how separated software or modules behave with each other.


# Unit testing in Python

The standard library provides two alternatives for unit testing:

-   [unittest](https://docs.python.org/3/library/unittest.html)
-   [doctest](https://docs.python.org/3/library/doctest.html?highlight=unittest)


## unittest

-   Standard
-   Class-based
-   Somewhat verbose to write


## doctest

-   Gathers test cases and expected results from the docstrings
-   Forces you to write docstrings

Do all know what a docstring is?


# Pytest

<https://docs.pytest.org/en/7.0.x/>

-   Easy (uses `assert` keyword)
-   Short, readable code for tests
-   Does not get in the way (test auto-recognition)


# Coverage

-   How much of your code are you testing?
-   Are you missing some test cases?


## A word of caution

Coverage isn't an objective indicator of the quality of your tests, it simply highlights cases you still need to cover.

Having 100% coverage doesn't mean there can't be bugs hiding in your code.


# Homework

Choose one of the following exercises and write a python script to solve it, complete with unit tests. It's up to you which library to use for testing. If you have the time, try solving all three using different libraries for testing.

Tip: refer to the [On-Line Encyclopedia of Integer Sequences](https://oeis.org) to make sure your code returns the correct Nth number in the sequence.


## 

Write a script that, given a number N, returns the Nth Fibonacci number. It shouldn't accept negative numbers, nor numbers that are too large (up to you to set the limit). It should only accept int, or float that are exact integers.


## 

Write a script that, given a number N, returns the Nth factorial number. It shouldn't accept negative numbers, nor numbers that are too large (up to you to set the limit). It should only accept int, or float that are exact integers.


## 

Write a script that, given a number N, returns the Nth prime number, It shouldn't accept negative numbers, nor numbers that are too large (up to you to set the limit). It should only accept int, or float that are exact integers.


# Testing FastAPI code

<https://fastapi.tiangolo.com/tutorial/testing/>